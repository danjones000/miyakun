# Miyakun

`miyakun` is a job queue runner based on Laravel Zero. It uses the standard
Laravel Queue, and RabbitMQ.

## What's with the name?

Miya-kun is named for [Miyamoto Usagi](https://en.wikipedia.org/wiki/Miyamoto_Usagi), 
the central character of the manga Usagi Yojimbo. the character is an
anthropmorphic rabbit ("usagi" is Japanese for "rabbit"), who is a samurai.

"kun" is a Japanese honorific used to indicate familiarity. It also sounds a
little bit like "queue".

Since the app uses RabbitMQ, it's named after a rabbit, and has a word that
sounds like "queue".
